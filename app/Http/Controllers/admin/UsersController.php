<?php

namespace App\Http\Controllers\admin;

use App\Helpers\MyImage;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('news.index');
        }
        return view('admin/users/login');
    }

    /**
     * Users authentication.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt(['login' => $request->login, 'password' => $request->password])) {
            return response()->json([
                'message' => "User was successfully login"
            ], 200);
        }

        return response()->json([
            'message' => "Wrong login or password"
        ], 422);
    }

    /**
     * User logout page.
     *
     * @return Redirect to HomePage
     */
    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return redirect('/');
    }


    public function profile()
    {
        $data = [
            'activePage' => 'profile',
            'record' => User::findOrFail(Auth::user()->id),
        ];

        return view('admin/users/profile', $data);
    }

    /**
     * Update user
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->validate($request, [
            'login'   => 'required|unique:users,login,' . $id,
            'name'    => 'required',
            'surname' => 'required',
            'email'   => 'required|unique:users,email,' . $id,
        ]);

        if (isset($input['avatar'])) {
            if (isset($input['avatar']) && !empty($input['old_avatar'])) {
                MyImage::deleteImage(public_path('images/users/'), $input['old_avatar']);
            }

            $input['avatar'] = MyImage::saveBase64Image($input['avatar'], public_path('images/users/'));
        }

        $user = User::findOrFail($id);
        $user->fill($input);
        $user->update();

        return response()->json([
            'message' => "Profile was updated"
        ], 200);
    }

    /**
     * Update user password
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changePassword(Request $request, $id)
    {
        $input = $request->all();

        $this->validate($request, [
            'password'     => 'required',
            'old_password' => 'required',
        ]);

        $user = User::findOrFail(Auth::user()->id);

        if (!Hash::check($input['old_password'], $user['password'])) {
            return response()->json([
                'message' => "Old password entered incorrectly"
            ], 422);
        }

        $user = User::findOrFail($id);
        $user->fill($input);
        $user->update();

        return response()->json([
            'message' => "The password has been successfully changed"
        ], 200);

    }
}
