<?php

namespace App\Http\Controllers\admin;

use App\Helpers\MyImage;
use App\Helpers\MyUrl;
use App\Http\Controllers\Controller;
use App\News;
use App\NewsCategory;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    private $imageWidth = 900;
    private $imageHeight = 500;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/news/index', ['activePage' => 'news']);
    }

    /**
     * @return JSON for Data table with all records
     */
    public function selectNews()
    {
        $query = News::select('id_news', 'title',  'description', 'image', 'id_category', 'created_at', 'is_must_read', 'is_published')->with('category');

        return datatables($query)
            ->order(function ($query) {
                $columns = [
                    0 => 'title',
                    5 => 'created_at'
                ];

                $dir = request()->order[0]['dir'];
                $col =  $columns[intval(request()->order[0]['column'])];

                $query->orderBy($col, $dir);
            })
            ->rawColumns(['actions', 'date_add', 'image', 'is_must_read', 'is_published', 'category', 'titleLink', 'categoryLink'])
            ->addColumn('actions', 'admin/news/partial/actions')
            ->addColumn('image', 'admin/news/partial/image')
            ->addColumn('is_must_read', 'admin/news/partial/is_must_read')
            ->addColumn('is_published', 'admin/news/partial/is_published')

            ->addColumn('date_add', function($query){
                return date('d.m.Y', strtotime($query->created_at));
            })

            ->addColumn('category', function($query){
                return $query->category['name'];
            })

            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/news/form', ['activePage' => 'news', 'categories' => NewsCategory::get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'        => 'required|min:3|max:100' ,
            'description'  => 'required|min:50|max:255',
            'id_category'  => 'required',
            'date_publish' => 'required',
            'content'      => 'required',
            'image'        => 'required',
        ]);

        $input = $request->all();

        $input['alias'] = MyUrl::makeSlugs($input['title']);
        $input['is_published'] = !isset($input['is_published']) ? 0 : 1;
        $input['is_must_read'] = !isset($input['is_must_read']) ? 0 : 1;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['image'] = uniqid() . '.' . $image->getClientOriginalExtension();

            MyImage::saveImage($image->getRealPath(), public_path('images/news/'),  $input['image'], $this->imageWidth, $this->imageHeight);
        }

        $article = new News();
        $article->fill($input);
        $article->save();

        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'News was added successfully');

        return response()->json([
            'status'  => 'success',
            'message' => 'News was added successfully',
            'redirect' => route('news.index'),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'activePage' => 'news',
            'categories' => NewsCategory::get(),
            'record'     => News::findOrFail($id)
        ];

        return view('admin/news/form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'        => 'required|min:3|max:100' ,
            'description'  => 'required|min:50|max:255',
            'id_category'  => 'required',
            'date_publish' => 'required',
            'content'      => 'required',
        ]);

        $input = $request->all();

        $input['alias'] = MyUrl::makeSlugs($input['title']);
        $input['is_published'] = !isset($input['is_published']) ? 0 : 1;
        $input['is_must_read'] = !isset($input['is_must_read']) ? 0 : 1;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $input['image'] = uniqid() . '.' . $image->getClientOriginalExtension();

            MyImage::saveImage($image->getRealPath(), public_path('images/news/'),  $input['image'], $this->imageWidth, $this->imageHeight);

            if (isset($input['old_image']) && !empty($input['old_image'])) {
                MyImage::deleteImage(public_path('images/news/'), $input['old_image']);
            }
        }

        $article = News::findOrFail($id);
        $article->fill($input);
        $article->save();

        $request->session()->flash('status', 'success');
        $request->session()->flash('message', 'News was updated successfully');

        return response()->json([
            'status'  => 'success',
            'message' => 'News was updated successfully',
            'redirect' => route('news.index'),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);

        if(isset($news['image']) && !empty($news['image'])){
            MyImage::deleteImage(public_path('images/news/'), $news['image']);
        }

        $news->delete();

        return response()->json([
            'status'  => 'success',
            'message' => 'The news has been deleted',
        ], 200);
    }

    /**
     * Make news published
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function isPublished($id)
    {
        $news = News::findOrFail($id);

        if ($news['is_published']) {
            $news->fill(['is_published' => 0]);
            $message = 'News was unpublished successfully';

        } else {
            $news->fill(['is_published' =>1]);
            $message = 'News was published successfully';
        }

        $news->update();

        return response()->json([
            'status'  => 'success',
            'message' => $message
        ], 200);
    }

    /**
     * Make news must read
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function isMustRead($id = 0)
    {
        $news = News::findOrFail($id);

        if ($news['is_must_read']) {
            $news->fill(['is_must_read' => 0]);
            $message = 'News is not "must read"';

        } else {
            $news->fill(['is_must_read' => 1]);
            $message = 'News was made "must reade"';
        }

        $news->update();

        return response()->json([
            'status'  => 'success',
            'message' => $message
        ], 200);
    }
}
