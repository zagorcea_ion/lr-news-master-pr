<?php

namespace App\Http\Controllers\admin;

use App\Helpers\MyUrl;
use App\Http\Controllers\Controller;
use App\NewsCategory;
use Illuminate\Http\Request;

class NewsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/news-category/index', ['activePage' => 'categories']);

    }

    /**
     * @return JSON for Data table with all records
     */
    public function selectCategories()
    {
        $query = NewsCategory::select('id_cat', 'name', 'created_at');

        return datatables($query)
            ->order(function ($query) {
                $columns = [
                    0 => 'name',
                    1 => 'created_at'
                ];

                $dir = request()->order[0]['dir'];
                $col =  $columns[intval(request()->order[0]['column'])];

                $query->orderBy($col, $dir);
            })

            ->rawColumns(['actions', 'date', 'name'])
            ->addColumn('actions', 'admin/news-category/partial/actions')

            ->addColumn('date', function($query){
                return date('d.m.Y', strtotime($query->created_at));
            })

            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/news-category/form', ['activePage' => 'categories']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name'=> 'required']);

        $input = $request->all();
        $input['alias'] = MyUrl::makeSlugs($input['name']);

        $category = new NewsCategory();
        $category->fill($input);
        $category->save();

        return response()->json([
            'status'  => 'success',
            'message' => "Category was added successfully"
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin/news-category/form', NewsCategory::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name'=> 'required']);

        $input = $request->all();
        $input['alias'] = MyUrl::makeSlugs($input['name']);

        $category = NewsCategory::findOrFail($id);;
        $category->fill($input);
        $category->update();

        return response()->json([
            'status'  => 'success',
            'message' => "Category was added updated"
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = NewsCategory::findOrFail($id);
        $category->delete();

        return response()->json([
            'status'  => 'success',
            'message' => 'The category has been deleted',
        ], 200);
    }
}
