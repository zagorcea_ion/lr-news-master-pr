<?php

namespace App\Http\Controllers\admin;

use App\Contact;
use App\Http\Controllers\Controller;


class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('admin/contact/index', ['activePage' => 'contact']);
    }

    /**
     * @return JSON for Data table with all records
     */
    public function selectContact()
    {
        $query = Contact::select('id_contact', 'email',  'name', 'phone', 'website', 'message', 'viewed', 'created_at');

        return datatables($query)
            ->order(function ($query) {
                $columns = [
                    5 => 'created_at'
                ];

                $dir = request()->order[0]['dir'];
                $col =  $columns[intval(request()->order[0]['column'])];

                $query->orderBy($col, $dir);
            })
            ->rawColumns(['name', 'email', 'phone', 'website', 'message', 'date_add', 'viewed', 'action'])
            ->addColumn('action', 'admin/contact/partial/actions')
            ->addColumn('viewed', 'admin/contact/partial/viewed')

            ->addColumn('date_add', function($query){
                return date('d.m.Y', strtotime($query->created_at));
            })

            ->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return response()->json([
            'message' => 'The contact message has been deleted',
        ], 200);
    }

    /**
     * Make contact message viewed
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function isViewed($id = 0)
    {
        $contact = Contact::findOrFail($id);

        if ($contact['viewed']) {
            $contact->fill(['viewed' => 0]);
            $message = 'Contact is not "viewed"';

        } else {
            $contact->fill(['viewed' => 1]);
            $message = 'Contact was made as "viewed"';
        }

        $contact->update();

        return response()->json([
            'status'  => 'success',
            'message' => $message
        ], 200);
    }

}
