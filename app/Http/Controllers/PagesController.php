<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Helpers\MyUrl;
use App\News;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PagesController extends Controller
{
    /**
     * Display home page.
     * @return Factory|View
     */
    public function homePage()
    {
        $data = [
            'news'         => News::isPublished()->take(10)->get(),
            'most_popular' => News::popular()->take(6)->get(),
            'must_read'    => News::mustRead()->take(6)->with('category')->get()
        ];

        return view('home-page', $data);
    }

    /**
     * Display news page.
     * @param string $category
     * @return Factory|View
     */
    public function newsPage($category = '')
    {

        if (!empty($category)) {
            $idCategory = MyUrl::getIdUrl($category);
            $news = News::where('id_category', $idCategory)->isPublished()->orderby('date_publish', 'desc')->paginate(10);

        } else {
            $news = News::isPublished()->orderby('date_publish', 'desc')->paginate(10);
        }

        $data = [
            'news'         => $news,
            'most_popular' => News::popular()->take(6)->get(),
        ];

        return view('news-page', $data);
    }

    /**
     * Display news detail page.
     * @param $alias
     * @return Factory|View
     */
    public function newsDetailPage($alias)
    {
        $id = MyUrl::getIdUrl($alias);

        $data = [
            'news'               => News::findOrFail($id),
            'most_popular'       => News::popular()->take(6)->get(),
            'can_be_interesting' => News::where('id_news', '!=', $id)->orderByRaw("RAND()")->take(3)->get(),
        ];

        return view('news-detail-page', $data);
    }

    /**
     * Display contact page.
     * @return Factory|View
     */
    public function contactPage()
    {
        return view('contact-page');
    }

    /**
     * Store contact message.
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\Response
     */
    public function storeContact(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required',
            'email'   => 'required|email',
            'phone'   => 'required',
            'message' => 'required',
        ]);

        $contact = new Contact();
        $contact->fill($request->all());
        $contact->save();

        return response()->json([
            'message' => "Contact message was added successfully"
        ], 200);
    }

}
