<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'name', 'phone', 'message', 'viewed'];

    /**
     * Set primary key for table.
     *
     * @var string
     */
    protected $primaryKey = 'id_contact';

}
