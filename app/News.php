<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'alias', 'description', 'id_category', 'date_publish', 'content', 'image', 'is_published', 'is_must_read'];

    /**
     * Set primary key for table.
     *
     * @var string
     */
    protected $primaryKey = 'id_news';

    /**
     * Get category for the news.
     */
    public function category()
    {
        return $this->hasOne('App\NewsCategory', 'id_cat', 'id_category');
    }

    /**
     * Scope a query to get only published news.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsPublished($query)
    {
        return $query->where('is_published', 1);
    }

    /**
     * Scope a query to only include popular news.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePopular($query)
    {
        return $query->isPublished()->orderBy('views', 'desc');
    }

    /**
     * Scope a query to get only must read news.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMustRead($query)
    {
        return $query->isPublished()->where('is_must_read', 1);
    }
}
