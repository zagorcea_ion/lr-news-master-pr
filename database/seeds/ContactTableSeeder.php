<?php

use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contact')->insert(
            [
                'email'      => 'zip14.05.1996@gmail.com',
                'name'       => 'Zagorcea Ion',
                'phone'      => '069621912',
                'message'    => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                'created_at' => date('Y-m-d H:i:s')
            ]
        );
    }
}
