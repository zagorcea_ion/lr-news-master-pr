@extends('layouts.admin')

@section('content')
    <div class="content-wrapper" style="min-height: 1015.13px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>News</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of all news</h3>

                            <div class="card-tools">
                                <a href="{{route('news.create')}}" class="btn btn-success"><i class="fas fa-plus"></i> Add news</a>
                                <a data-fancybox data-type="ajax" data-src="{{route('category.create')}}" href="javascript:;" class="btn btn-success"><i class="fas fa-plus"></i> Add category</a>
                            </div>

                        </div>

                        @if (Session::has('status') && Session::has('message'))
                            <script>
                                $(document).ready(function() {
                                    sweetalert('{{Session::get('status')}}', 'Success', '{{Session::get('message')}}');

                                })
                            </script>
                        @endif

                        <!-- /.card-header -->
                        <div class="card-body">
                            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12">
                                        <table id="categories-table" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                            <thead>
                                            <tr role="row">
                                                <th>Title</th>
                                                <th>Category</th>
                                                <th>Image</th>
                                                <th>Published</th>
                                                <th>Must read</th>
                                                <th>Date add</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->

                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>



    <script>
        var dt;

        $(document).ready(function() {

            dt = $('#categories-table').DataTable( {
                "processing": true,
                "serverSide": true,
                "searching": true,
                "autoWidth": false,
                responsive: true,
                stateSave: true,
                "order": [ 5, 'desc' ],
                "columnDefs": [
                    {"orderable": true, "targets": 0},
                    {"orderable": false, className: "ta-c w-200", "targets": 1},
                    {"orderable": false, className: "ta-c w-100", "targets": 2},
                    {"orderable": false, className: "ta-c w-100", "targets": 3},
                    {"orderable": false, className: "ta-c w-100", "targets": 4},
                    {"orderable": true, className: "ta-c w-100", "targets": 5},
                    {"orderable": false, className: "ta-c w-200", "targets": 6},
                ],
                "columns": [
                    { "data": "title"},
                    { "data": "category"},
                    { "data": "image"},
                    { "data": "is_published"},
                    { "data": "is_must_read"},
                    { "data": "date_add" },
                    { "data": "actions" },
                ],
                "ajax": {
                    url: "{{route('news.list')}}",
                    type: 'POST',
                }

            });
        });
    </script>

@endsection
