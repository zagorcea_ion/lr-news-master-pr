@extends('layouts.admin')

@section('content')
    <script src="{{asset('plugins/tinymce/tinymce.min.js')}}"></script>

    <div class="content-wrapper" style="min-height: 1015.13px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>News</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Add news</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form id="news-form" action="{{!empty($record['id_news']) ? route('news.update', ['news' => $record['id_news']]) : route('news.store')}}" method="post" enctype="multipart/form-data">
                                @if (!empty($record['id_news']))
                                    <input type="hidden" name="_method" value="PUT">
                                @endif

                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control validate[required]" id="title" name="title" value="{{!empty($record['title']) ? $record['title'] : ''}}" placeholder="Title:">
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <textarea class="form-control validate[required]" id="description" name="description" rows="3" placeholder="Description:">{{!empty($record['description']) ? $record['description'] : ''}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="id-category">Category</label>
                                        <select class="form-control" id="id-category" name="id_category">
                                            <option selected disabled>Select category</option>

                                            @if (!empty($categories))
                                                @foreach ($categories as $category)
                                                    <option value="{{$category['id_cat']}}" {{!empty($record['id_category']) && $record['id_category'] == $category['id_cat'] ? 'selected' : ''}}>
                                                        {{$category['name']}}
                                                    </option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="date-publish">Date publish</label>
                                        <input type="date" class="form-control validate[required]" id="date-publish" name="date_publish" value="{{!empty($record['date_publish']) ? date('Y-m-d', strtotime($record['date_publish'])) : ''}}">
                                    </div>

                                    <div class="form-group">
                                        <label for="content">Content</label>
                                        <textarea class="form-control validate[required]" id="content" name="content" placeholder="Content:">{{!empty($record['content']) ? $record['content'] : ''}}</textarea>
                                    </div>

                                    @if (!empty($record['image']))
                                        <input type="hidden" name="old_image" value="{{$record['image']}}">
                                        <div class="form-group">
                                            <img src="{{asset('images/news/') . '/' . $record['image']}}" width="200px">
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control {{empty($record['id_news']) ? 'validate[required]' : ''}}" id="image" name="image" placeholder="Image:">
                                    </div>

                                    <div class="form-group">
                                        <label>Published</label>
                                        <input id="published" name="is_published" type="checkbox" {{!empty($record['is_published']) && $record['is_published'] ?  'checked'  : ''}}>
                                        <label class="switch-color" for="published"></label>
                                    </div>

                                    <div class="form-group">
                                        <label>Must read</label>
                                        <input id="must-read" name="is_must_read" type="checkbox" {{!empty($record['is_must_read']) && $record['is_must_read'] ?  'checked'  : ''}}>
                                        <label class="switch-color" for="must-read"></label>
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
        </section>
        <!-- /.content -->
    </div>

    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: '#content',
                entity_encoding: 'raw',
                menubar: false,
                branding: false,
                height: 500,
                max_height: 700,
                min_height: 500,
                plugins: [
                    'autolink autoresize fullscreen link lists paste',
                ],
                toolbar: 'undo redo | bold italic | underline strikethrough | bullist numlist | link | fullscreen',

                link_assume_external_targets: true,
                relative_urls: false,
                image_advtab: true ,
                remove_script_host: false,
                force_br_newlines: false,
                force_p_newlines: false,
                forced_root_block: "",
                extended_valid_elements: "br",
                verify_html: false,
                valid_children: "br",
                paste_as_text: true,

            });

            var validation = $("#news-form").validationEngine('attach',{promptPosition : "bottomLeft", maxErrorsPerField: 1, validateNonVisibleFields: true, updatePromptsPosition:true});

            $('#news-form').on('submit', function (e) {
                e.preventDefault();
                if(!validation.validationEngine('validate')){
                    return false;
                }

                var formData = new FormData(this);

                $.ajax({
                    url : $(this).attr('action'),
                    type : 'POST',
                    dataType : 'JSON',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data : formData,
                    success : function(response){
                        if (typeof response.redirect != 'undefined' && typeof response.redirect != null) {
                            window.location.replace(response.redirect);
                        }
                    },
                    error: function (error) {
                        onSaveRequestError(error);
                    }
                });
            });
        });

    </script>

@endsection
