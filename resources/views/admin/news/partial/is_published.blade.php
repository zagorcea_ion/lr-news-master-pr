<input class="change-flag" id="published-{{$id_news}}" data-url="{{route('news.isPublished', ['id' => $id_news])}}" type="checkbox" {{$is_published ? "checked" : " "}}>
<label class="switch-color" for="published-{{$id_news}}"></label>
