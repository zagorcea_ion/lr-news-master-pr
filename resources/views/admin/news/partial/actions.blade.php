<a href="{{ route('news.edit', $id_news) }}" class="btn btn-primary">
    <i class="fas fa-edit"></i> Edit
</a>

<button type="button" class="btn btn-danger delete" data-text="Do you want delete this category?" data-url="{{ route('news.destroy', $id_news) }}">
    <i class="fas fa-trash-alt"></i> Delete
</button>
