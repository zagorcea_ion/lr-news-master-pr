<button type="button" class="btn btn-danger delete" data-text="Do you want delete this contact?" data-url="{{route('contact.destroy', ['id' => $id_contact])}}">
    <i class="fas fa-trash-alt"></i> Delete
</button>
