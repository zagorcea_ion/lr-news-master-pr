@extends('layouts.admin')

@section('content')
    <div class="content-wrapper" style="min-height: 1015.13px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Contact message</h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List of all message</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12">
                                        <table id="contact-table" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                            <thead>
                                            <tr role="row">
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Website</th>
                                                <th>Message</th>
                                                <th>Date add</th>
                                                <th>Viewed</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->

                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <script>
        var dt;

        $(document).ready(function() {

            dt = $('#contact-table').DataTable( {
                "processing": true,
                "serverSide": true,
                "autoWidth": false,
                "searching": false,
                responsive: true,
                stateSave: true,
                "order": [ 5, 'desc' ],
                "columnDefs": [
                    {"orderable": false, className: "ta-c w-100", "targets": 0},
                    {"orderable": false, className: "ta-c w-100", "targets": 1},
                    {"orderable": false, className: "ta-c w-50", "targets": 2},
                    {"orderable": false, className: "ta-c w-100", "targets": 3},
                    {"orderable": false, "targets": 4},
                    {"orderable": true, className: "ta-c w-70", "targets": 5},
                    {"orderable": false, className: "ta-c w-70", "targets": 6},
                    {"orderable": false, className: "ta-c w-70", "targets": 7},
                ],
                "columns": [
                    { "data": "name"},
                    { "data": "email"},
                    { "data": "phone"},
                    { "data": "website"},
                    { "data": "message"},
                    { "data": "date_add" },
                    { "data": "viewed" },
                    { "data": "action" },
                ],
                "ajax": function (data, callback, settings) {
                    $.ajax({
                        "dataType": 'JSON',
                        "type": "POST",
                        "data": data,
                        url: "{{route('contact.list')}}",
                        "success": function (data, textStatus, jqXHR) {
                            callback(data, textStatus, jqXHR);
                        }
                    });
                },

            });
        });
    </script>

@endsection
