<a data-fancybox data-type="ajax" data-src="{{ route('category.edit', $id_cat) }}" href="javascript:;" class="btn btn-primary">
    <i class="fas fa-edit"></i> Edit
</a>

<button type="button" class="btn btn-danger delete" data-text="Do you want delete this category?" data-url="{{ route('category.destroy', $id_cat) }}">
    <i class="fas fa-trash-alt"></i> Delete
</button>
