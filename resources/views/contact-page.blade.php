@extends('layouts.public')

@section('content')
    <!-- Start page content -->
    <section id="page-content" class="page-wrapper">
        <!-- Start contact address area  -->
        <div class="zm-section bg-white ptb-65">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4 col-sm-8">
                        <div class="section-title-2 mb-40">
                            <h3 class="inline-block uppercase">Contact Us</h3>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="single-address text-center">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            <h4>Address</h4>
                            <p>str.Puskin, Balti, Moldova</p>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 xs-mt-30">
                        <div class="single-address text-center">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <h4 class="uppercase">Email Address</h4>
                            <p>test@admin.com</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 xs-mt-30">
                        <div class="single-address text-center">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <h4 class="uppercase">Phone Number</h4>
                            <p>+373 688 30 059</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End contact address area  -->
        <!-- Start Google Map area -->
        <div class="zm-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="google-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1341.1688140573879!2d27.921054512065833!3d47.755505158688095!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDfCsDQ1JzE5LjgiTiAyN8KwNTUnMTguMyJF!5e0!3m2!1sru!2s!4v1578487025923!5m2!1sru!2s" width="100%" height="600px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Google Map area -->
        <!-- Start contact message area -->
        <div class="zm-section bg-white pt-60 pb-40">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <div class="section-title-2 mb-40">
                            <h3 class="inline-block uppercase">Send Us A MEssage</h3>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                        </div>
                    </div>
                </div>
                <div class="message-box">
                    <form action="{{route('pages.storeContact')}}" id="contact-form" method="post">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="validate[required]" name="name" placeholder="Full Name*">
                                <input type="text" class="validate[required,custom[email]]" name="email" placeholder="Email Address*">
                                <input type="text" class="validate[required]" name="phone" placeholder="Phone Number">
                                <input type="text" class="validate[custom[url]]" name="website" placeholder="Website">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="message" class="validate[required]" placeholder="Type your message..."></textarea>
                                <button class="submit-button" type="submit">Send Message</button>
                            </div>
                        </div>
                    </form>
                    <p class="form-messege"></p>
                </div>
            </div>
        </div>
        <!-- End contact message area -->
    </section>
    <!-- End page content -->

    <script>
        $(document).ready(function() {
            var validation = $("#contact-form").validationEngine('attach',{promptPosition : "bottomLeft", maxErrorsPerField: 1, validateNonVisibleFields: true, updatePromptsPosition:true});

            $('#contact-form').on('submit', function (e) {
                e.preventDefault();
                if(!validation.validationEngine('validate')){
                    return false;
                }

                var formData = new FormData(this);

                $.ajax({
                    url : $(this).attr('action'),
                    type : 'POST',
                    dataType : 'JSON',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data : formData,
                    success : function(response){
                        $('#contact-form')[0].reset();
                        sweetalert('success', 'Success', response.message);

                    },
                    error: function (error) {
                        onSaveRequestError(error);
                    }
                });
            });
        });

    </script>

@endsection
