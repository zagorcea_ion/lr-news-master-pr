@extends('layouts.public')

@section('content')
    <!-- Start page content -->
    <div id="page-content" class="page-wrapper">
        <div class="zm-section single-post-wrap bg-white ptb-70 xs-pt-30">
            <div class="container">
                <div class="row">
                    <!-- Start left side -->
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 columns">

                        <div class="row">
                            <!-- Start single post image formate-->
                            <div class="col-md-12">
                                <article class="zm-post-lay-single">
                                    <div class="zm-post-thumb">
                                        <img src="{{asset('images/news/' . $news['image'])}}" alt="{{$news['title']}}">
                                    </div>
                                    <div class="zm-post-dis">
                                        <div class="zm-post-header">
                                            <h2 class="zm-post-title h2">{{$news['title']}}</h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta">Views: {{$news['views']}}</li>
                                                    <li class="s-meta">{{date('F d, Y', strtotime($news['date_publish']))}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="zm-post-content">
                                            {!! $news['content'] !!}
                                        </div>


                                    </div>
                                </article>
                            </div>

                            @if (!empty($can_be_interesting))
                                <!--Start Similar post -->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ptb-40 mtb-40 border-top">
                                    <aside class="zm-post-lay-a2-area">
                                        <div class="post-title mb-40">
                                            <h2 class="h6 inline-block">Can be intresting</h2>
                                        </div>
                                        <div class="row">
                                            <div class="zm-posts clearfix">
                                                @foreach ($can_be_interesting as $item)
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                                        <article class="zm-post-lay-a2">
                                                            <div class="zm-post-thumb">
                                                                <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">
                                                                    <img src="{{asset('images/news/' . $item['image'])}}" alt="{{$item['title']}}">
                                                                </a>
                                                            </div>
                                                            <div class="zm-post-dis">
                                                                <div class="zm-post-header">
                                                                    <h2 class="zm-post-title h2">
                                                                        <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">{{$item['title']}}</a>
                                                                    </h2>
                                                                    <div class="zm-post-meta">
                                                                        <ul>
                                                                            <li class="s-meta">Views: {{$item['views']}}</li>
                                                                            <li class="s-meta">{{date('F d, Y', strtotime($item['date_publish']))}}</li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <!-- End similar post -->
                            @endif

                        </div>
                    </div>
                    <!-- End left side -->

                    @if (!empty($most_popular))
                        <!-- Start Right sidebar -->
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar-warp columns">
                            <div class="row">
                                <!-- Start post layout E -->
                                <aside class="zm-post-lay-e-area col-sm-6 col-md-12 col-lg-12">
                                    <div class="row mb-40">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="section-title">
                                                <h2 class="h6 header-color inline-block uppercase">Most Popular</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="zm-posts">
                                                @foreach ($most_popular as $item)
                                                    <!-- Start single post layout E -->
                                                    <article class="zm-post-lay-e zm-single-post hidden-sm hidden-md clearfix">
                                                        <div class="zm-post-thumb f-left">
                                                            <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">
                                                                <img src="{{asset('images/news/' . $item['image'])}}" alt="{{$item['title']}}">
                                                            </a>
                                                        </div>
                                                        <div class="zm-post-dis f-right">
                                                            <div class="zm-post-header">
                                                                <h2 class="zm-post-title">
                                                                    <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">{{$item['title']}}</a>
                                                                </h2>
                                                                <div class="zm-post-meta">
                                                                    <ul>
                                                                        <li class="s-meta">Views: {{$item['views']}}</li>
                                                                        <li class="s-meta">{{date('F d, Y', strtotime($item['date_publish']))}}</li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                    <!-- Start single post layout E -->

                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </aside>
                                <!-- End post layout E -->

                            </div>
                        </div>
                        <!-- End Right sidebar -->

                    @endif

                </div>
            </div>
        </div>
    </div>
    <!-- End page content -->

@endsection
