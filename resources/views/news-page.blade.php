@extends('layouts.public')

@section('content')
    <section id="page-content" class="page-wrapper">
        <div class="zm-section bg-white pt-70 pb-40">
            <div class="container">
                <div class="row">
                    <!-- Start left side -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 columns">
                        <div class="row mb-40">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="section-title">
                                    <h2 class="h6 header-color inline-block uppercase">Latest News</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="zm-posts">

                                    @foreach ($news as $item)
                                        <article class="zm-post-lay-c zm-single-post clearfix">
                                            <div class="zm-post-thumb f-left">
                                                <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">
                                                    <img src="{{asset('images/news/' . $item['image'])}}" alt="{{$item['title']}}">
                                                </a>
                                            </div>
                                            <div class="zm-post-dis f-right">
                                                <div class="zm-post-header">
                                                    <div class="zm-category">
                                                        <a href="{{route('pages.categoryNews', ['category' => $item->category['id_cat'] . '-' . $item->category['alias']])}}" class="bg-cat-5 cat-btn">{{$item->category['name']}}</a>
                                                    </div>
                                                    <h2 class="zm-post-title">
                                                        <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">{{$item['title']}}</a>
                                                    </h2>
                                                    <div class="zm-post-meta">
                                                        <ul>
                                                            <li class="s-meta">Views: {{$item['views']}}</li>
                                                            <li class="s-meta">{{date('F d, Y', strtotime($item['date_publish']))}}</li>
                                                        </ul>
                                                    </div>
                                                    <div class="zm-post-content">
                                                        <p>{{$item['description']}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </article>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End left side -->
                    @if (!empty($most_popular))
                        <!-- Start Right sidebar -->
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 sidebar-warp columns">
                                <div class="row">
                                    <!-- Start post layout E -->
                                    <aside class="zm-post-lay-e-area col-xs-12 col-sm-6 col-md-6 col-lg-12 hidden-md">
                                        <div class="row mb-40">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="section-title">
                                                    <h2 class="h6 header-color inline-block uppercase">Most Popular</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="zm-posts">
                                                    @foreach ($most_popular as $item)
                                                        <!-- Start single post layout E -->
                                                        <article class="zm-post-lay-e zm-single-post hidden-sm hidden-md clearfix">
                                                            <div class="zm-post-thumb f-left">
                                                                <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">
                                                                    <img src="{{asset('images/news/' . $item['image'])}}" alt="{{$item['title']}}">
                                                                </a>
                                                            </div>
                                                            <div class="zm-post-dis f-right">
                                                                <div class="zm-post-header">
                                                                    <h2 class="zm-post-title">
                                                                        <a href="{{route('pages.detailNews', ['news' => $item['id_news'] . '-' . $item['alias']])}}">{{$item['title']}}</a>
                                                                    </h2>
                                                                    <div class="zm-post-meta">
                                                                        <ul>
                                                                            <li class="s-meta">Views: {{$item['views']}}</li>
                                                                            <li class="s-meta">{{date('F d, Y', strtotime($item['date_publish']))}}</li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <!-- Start single post layout E -->
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                    <!-- Start post layout E -->
                                </div>
                            </div>
                            <!-- End Right sidebar -->
                    @endif

                </div>
                <!-- Start pagination area -->
                <div class="row hidden-xs">
                    <div class="zm-pagination-wrap mt-70">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <nav class="zm-pagination ptb-40 text-center">
                                        {{ $news->links("layouts/paginate") }}
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End pagination area -->
            </div>
        </div>
    </section>
    <!-- End page content -->

@endsection
