<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Znews | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.ico')}}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">

    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/croppie/croppie.css')}}" />

    <link rel="stylesheet" href="{{asset('plugins/fancybox/dist/jquery.fancybox.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/admin-css.css')}}" />
    <link rel="stylesheet" href="{{asset('css/sizes.css')}}" />


    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="{{asset('plugins/Validation-Engine/validationEngine.jquery.css')}}" rel="stylesheet" type="text/css" />
    <!-- jQuery -->
    <script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>

    <script src="{{asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <img src="{{asset('AdminLTE/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light"><strong>Z</strong>NEWS</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">

                    @if (!empty(Auth::user()->avatar))
                        <img src="{{asset('images/users') . '/' . Auth::user()->avatar}}" class="img-circle elevation-2" alt="{{Auth::user()->surname . ' ' . Auth::user()->name}}">
                    @else
                        <img src="{{asset('images/no_avatar.png')}}" class="img-circle elevation-2" alt="{{Auth::user()->surname . ' ' . Auth::user()->name}}">
                    @endif


                </div>
                <div class="info">
                    <a href="#" class="d-block">{{Auth::user()->surname . ' ' . Auth::user()->name}}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    <li class="nav-item">
                        <a href="{{route('category.index')}}" class="nav-link {{!empty($activePage) && $activePage == 'categories' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-list"></i>
                            <p>Categories</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('news.index')}}" class="nav-link {{!empty($activePage) && $activePage == 'news' ? 'active' : ''}}">
                            <i class="nav-icon far fa-newspaper"></i>
                            <p>News</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('contact.index')}}" class="nav-link {{!empty($activePage) && $activePage == 'contact' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-envelope"></i>
                            <p>Contact message</p>
                        </a>
                    </li>

                    <li class="nav-header">USER</li>

                    <li class="nav-item">
                        <a href="{{route('user.profile')}}" class="nav-link {{!empty($activePage) && $activePage == 'profile' ? 'active' : ''}}">
                            <i class="nav-icon far fa-user"></i>
                            <p>Profile</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="logout" data-text="Do you want to leave the admin-panel?" data-url="{{route('logout')}}">
                            <i class="nav-icon fas fa-arrow-alt-circle-left"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>


    @yield('content')


    <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2014-2019<a href="http://adminlte.io">AdminLTE.io</a>.</strong>
                All rights reserved.
                <div class="float-right d-none d-sm-inline-block">
                    <b>Version</b> 3.0.0-rc.1
                </div>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <!-- jQuery UI 1.11.4 -->
        <script src="{{asset('AdminLTE/plugins/jquery-ui/jquery-ui.min.js   ')}}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button)
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

        <script src="{{asset('AdminLTE/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

        <script src="{{asset('AdminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('AdminLTE/dist/js/adminlte.js')}}"></script>


        <script src="{{asset('plugins/Validation-Engine/jquery.validationEngine.js')}}"></script>
        <script src="{{asset('plugins/Validation-Engine/jquery.validationEngine-en.js')}}"></script>

        <script src="{{asset('AdminLTE/plugins/croppie/croppie.js')}}"></script>
        <script src="{{asset('plugins/fancybox/dist/jquery.fancybox.min.js')}}"></script>
        <script src="{{asset('js/admin-js.js')}}"></script>

    </body>
</html>
