@if ($paginator->hasPages())
    <ul class="page-numbers">
        <!-- Previous Page Link -->
        @if (!$paginator->onFirstPage())
            <li class="prev page-numbers"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">Previous</a></li>
        @endif

        <!-- Pagination Elements -->
        @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

        <!-- Array Of Links -->
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li><span class="page-numbers current">{{ $page }}</span></li>
                    @else
                        <li class="page-numbers"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        <!-- Next Page Link -->
        @if ($paginator->hasMorePages())
            <li class="next page-numbers"><a href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a></li>
        @endif
    </ul>

@endif

