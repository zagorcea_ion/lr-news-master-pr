<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Znews - IT news </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favicon.ico')}}">

    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="{{asset('css/core.css')}}">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="{{asset('css/shortcode/shortcodes.css')}}">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <!-- User style -->
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    <link href="{{asset('plugins/Validation-Engine/validationEngine.jquery.css')}}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

    <!-- jQuery -->
    <script src="{{asset('AdminLTE/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Modernizr JS -->
    <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>
<body>

<!--  THEME PRELOADER AREA -->
<div id="preloader-wrapper">
    <div class="preloader-wave-effect"></div>
</div>
<!-- THEME PRELOADER AREA END -->

<!-- Body main wrapper start -->
<div class="wrapper">
    <!-- Start of header area -->
    <header  class="header-area header-wrapper bg-white clearfix">
        <!-- Start Sidebar Menu -->
        <div class="sidebar-menu">
            <div class="sidebar-menu-inner"></div>
            <span class="fa fa-remove"></span>
        </div>
        <!-- End Sidebar Menu -->

        <div class="header-middle-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-5 col-xs-12 header-mdh">
                        <div class="global-table">
                            <div class="global-row">
                                <div class="global-cell">
                                    <div class="logo">
                                        <a href="{{route('pages.home')}}">
                                            <img src="{{asset('images/logo/1.png')}}" alt="main logo">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="sticky-header" class="header-bottom-area hidden-sm hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="menu-wrapper  bg-theme clearfix">
                            <div class="row">
                                <div class="col-md-11">
                                    <div class="mainmenu-area">
                                        <nav class="primary-menu uppercase">
                                            <ul class="clearfix">
                                                <li><a href="{{route('pages.home')}}">Home</a></li>
                                                <li><a href="{{route('pages.news')}}">News</a></li>
                                                <li><a href="{{route('pages.contact')}}">Contact</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- mobile-menu-area start -->
        <div class="mobile-menu-area hidden-md hidden-lg">
            <div class="fluid-container">
                <nav id="mobile_dropdown">
                    <ul>
                        <li><a href="{{route('pages.home')}}">Home</a></li>
                        <li><a href="{{route('pages.news')}}">News</a></li>
                        <li><a href="{{route('pages.contact')}}">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- mobile-menu-area end -->
    </header>
    <!-- End of header area -->

@yield('content')

<!-- Start footer area -->
<footer id="footer" class="footer-wrapper footer-1">
    <!-- Start footer top area -->
    <div class="footer-top-wrap ptb-70 bg-dark">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 hidden-sm">
                    <div class="zm-widget pr-40">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">About Znews</h2>
                        <div class="zm-widget-content">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.</p>
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-6 col-lg-3">
                    <div class="zm-widget">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">Social Links</h2>
                        <div class="zm-widget-content">
                            <div class="zm-social-media zm-social-1">
                                <ul>
                                    <li><a href="https://www.facebook.com"><i class="fa fa-facebook"></i>Like us on Facebook</a></li>
                                    <li><a href="https://twitter.com"><i class="fa fa-twitter"></i>Tweet us on Twitter</a></li>
                                    <li><a href="https://www.pinterest.com/"><i class="fa fa-pinterest"></i>Pin us on Pinterest</a></li>
                                    <li><a href="https://www.instagram.com/"><i class="fa fa-instagram"></i>Heart us on Instagram</a></li>
                                    <li><a href="https://plus.google.com"><i class="fa fa-google-plus"></i>Share us on GooglePlus</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-6 col-lg-4">
                    <div class="zm-widget">
                        <h2 class="h6 zm-widget-title uppercase text-white mb-30">Contact</h2>
                        <!-- Start Subscribe From -->
                        <div class="zm-widget-content">
                            <div class="subscribe-form subscribe-footer">
                                <p>If you have any questions you can contact us.</p>
                                <a href="{{route('pages.contact')}}" class="contact-us"> Contact us</a>
                            </div>
                        </div>
                        <!-- End Subscribe From -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer top area -->
    <div class="footer-buttom bg-black ptb-15">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="zm-copyright text-center">
                        <p class="uppercase">© <?=date('Y')?> Your Company. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End footer area -->
</div>
<!-- Body main wrapper end -->

<!-- Placed js at the end of the document so the pages load faster -->
<!-- jquery latest version -->
<script src="{{asset('js/vendor/jquery-1.12.1.min.js')}}"></script>
<!-- Bootstrap framework js -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- All js plugins included in this file. -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/plugins.js')}}"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
<script src="{{asset('js/main.js')}}"></script>

<script src="{{asset('AdminLTE/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script src="{{asset('plugins/Validation-Engine/jquery.validationEngine.js')}}"></script>
<script src="{{asset('plugins/Validation-Engine/jquery.validationEngine-en.js')}}"></script>

<script src="{{asset('js/public.js')}}"></script>


</body>
</html>

