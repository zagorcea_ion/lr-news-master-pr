var sweetalert = function (type, title, message) {
    Swal.fire({
        type: type,
        title: title,
        text: message,
    })

}

//Confirm event
$('body').on('click', '.confirm', function () {
    Swal.fire({
        title: 'Are you sure?',
        text: $(this).data('text'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url : $(this).data('url'),
                type : 'POST',
                dataType : 'JSON',
                success : function(response){
                    sweetalert('success', 'Success', response.message);

                    if (typeof dt != null && typeof dt != 'undefined') {
                        dt.draw(false);
                    }
                },
                error: function (error) {
                    onSaveRequestError(error);
                }
            });
        }
    })

});

//Confirm event
$('body').on('click', '.delete', function () {
    Swal.fire({
        title: 'Are you sure?',
        text: $(this).data('text'),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url : $(this).data('url'),
                type : 'POST',
                dataType: "JSON",
                data: {
                    "_method": 'DELETE',
                },
                success : function(response){
                    sweetalert('success', 'Success', response.message);

                    if (typeof dt != null && typeof dt != 'undefined') {
                        dt.draw(false);
                    }
                },
                error: function (error) {
                    onSaveRequestError(error);
                }
            });
        }
    })

});

//Change flag event
$('body').on('change', '.change-flag', function () {

    var url = $(this).data('url');
    console.log(url);
    $.ajax({
        dataType: 'JSON',
        method: 'POST',
        url: $(this).data('url'),

        success : function(response){
            if(response.status == 'success'){
                sweetalert('success', 'Success', response.message);
            }

            if (typeof dt != null && typeof dt != 'undefined') {
                dt.draw(false);
            }
        },
        error: function (error) {
            onSaveRequestError(error);
        }
    });
})

//Logout event
$('body').on('click', '#logout', function () {
	Swal.fire({
		title: 'Are you sure?',
		text: $(this).data('text'),
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
			window.location.replace($(this).data('url'));
		}
	})
});

(function(window, $) {
    let token = document.head.querySelector('meta[name="csrf-token"]');
    if (token) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token.content,
                'X-Shenanigans': true,
            }
        })
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

    $(".close-alert").click(function () {
        $('#message-box-success').hide();
    });
    $("#message-box-danger").click(function () {
        $('.message-box-danger').removeClass("show");
        $('.message-box-danger').addClass("hide");
    });
} (window, jQuery));

function onSaveRequestError(error) {

    if(!error.hasOwnProperty('responseJSON')) {
        sweetalert('error', 'Oops...', 'Bad response');
        return;
    }

    var responseBody = error.responseJSON || {};
    if(responseBody.hasOwnProperty('errors')) {
        for (var key in responseBody.errors) {
            if (responseBody.errors.hasOwnProperty(key)) {
                var errorEntry = responseBody.errors[key];

                if(Array.isArray(errorEntry)) {
                    errorEntry.forEach(function(entry) {
                        sweetalert('error', 'Oops...', entry);

                    });

                } else {
                    sweetalert('error', 'Oops...', errorEntry);
                }
            }
        }
    } else if(responseBody.hasOwnProperty('message')){
        sweetalert('error', 'Oops...', responseBody.message);

    } else {
        sweetalert('error', 'Oops...', 'Server error');
    }
};



