function sweetalert(type, title, message) {
    Swal.fire({
        type: type,
        title: title,
        text: message,
    })

}

(function(window, $) {
    let token = document.head.querySelector('meta[name="csrf-token"]');
    if (token) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token.content,
                'X-Shenanigans': true,
            }
        })
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

} (window, jQuery));





