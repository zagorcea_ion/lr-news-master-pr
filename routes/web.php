<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@homePage')->name('pages.home');
Route::get('news', 'PagesController@newsPage')->name('pages.news');
Route::get('news/category/{category}', 'PagesController@newsPage')->name('pages.categoryNews');
Route::get('news/{news}', 'PagesController@newsDetailPage')->name('pages.detailNews');

Route::get('contact', 'PagesController@contactPage')->name('pages.contact');
Route::post('store-contact', 'PagesController@storeContact')->name('pages.storeContact');

//User login & logout
Route::get('login', 'admin\UsersController@login')->name('login');
Route::post('authenticate', 'admin\UsersController@authenticate')->name('authenticate');
Route::get('logout', 'admin\UsersController@logout')->name('logout');


Route::group(['prefix'=>'admin-panel', 'middleware'=>'auth'], function (){
    //News
    Route::post('news/select', 'admin\NewsController@selectNews')->name('news.list');
    Route::post('news/is-published/{id}', 'admin\NewsController@isPublished')->name('news.isPublished');
    Route::post('news/is-must-read/{id}', 'admin\NewsController@isMustRead')->name('news.isMustRead');
    Route::resource('news', 'admin\NewsController');

    //Category
    Route::post('category/select', 'admin\NewsCategoryController@selectCategories')->name('category.list');
    Route::resource('category', 'admin\NewsCategoryController');

    //User
    Route::get('user/profile', 'admin\UsersController@profile')->name('user.profile');
    Route::post('user/update/{user}', 'admin\UsersController@update')->name('user.update');
    Route::post('user/change-password/{user}', 'admin\UsersController@changePassword')->name('user.change-password');

    //Contact
    Route::get('contact', 'admin\ContactController@index')->name('contact.index');
    Route::post('contact/select', 'admin\ContactController@selectContact')->name('contact.list');
    Route::post('contact/is-viewed/{id}', 'admin\ContactController@isViewed')->name('contact.isViewed');

    Route::delete('contact/{id}', 'admin\ContactController@destroy')->name('contact.destroy');



});
